let currentPage = 1
let cardsData = []
let cardsLimit = 0
let darkTheme = false
const cardIncrease = 4;

const getData = async () => {
    return await fetch('../data.json')
        .then((response) => response.json())
        .then((json) => json);
}

const createCard = (card, index) => {
    const { image, caption, source_type, liked, date, likes, name, profile_image } = card
    const cardholder = document.createElement('div')
    cardholder.classList.add('cardholder')
    if (darkTheme === true) {
        cardholder.classList.add('themeBlack')
    }
    if (darkTheme === false) {
        cardholder.classList.add('themeWhite')
    }
    const layoutplaceholder = document.querySelector(".layout-placeholder")
    layoutplaceholder.append(cardholder)

    const personalInfo = document.createElement('div')
    personalInfo.classList.add('personalinfo')
    cardholder.append(personalInfo)

    const flexgrid = document.createElement('div')
    flexgrid.classList.add(`dflex`, `jccSB`)
    personalInfo.append(flexgrid)

    const personalPhoto = document.createElement('div')
    personalPhoto.classList.add(`dflex`, `jccStart`, `personalPhoto`)
    const createImg = document.createElement("img")
    createImg.setAttribute("src", profile_image)


    const paragraphHolder = document.createElement('div')
    const paragraphTagName = document.createElement('p')
    const paragraphTagDate = document.createElement('p')
    paragraphTagName.innerText = name
    paragraphTagDate.innerText = date
    paragraphTagName.classList.add(`paddingLeft1rem`, `name`)
    paragraphTagDate.classList.add(`paddingLeft1rem`, `date`)

    paragraphHolder.append(paragraphTagName, paragraphTagDate)
    personalPhoto.append(createImg)
    personalPhoto.append(paragraphHolder)

    const logoHolder = document.createElement('div')
    const createLogoImg = document.createElement("img")
    createLogoImg.setAttribute("src", source_type === "facebook" ? "../icons/facebook.svg" : "../icons/instagram-logo.svg")
    createLogoImg.classList.add(`socialLogo`)
    logoHolder.append(createLogoImg)
    flexgrid.append(personalPhoto)
    flexgrid.append(logoHolder)

    const cardImage = document.createElement('div')
    const createCardImgTag = document.createElement("img")
    createCardImgTag.setAttribute("src", image)
    cardImage.classList.add(`imgholder`)
    cardImage.append(createCardImgTag)
    cardholder.append(cardImage)

    const cardDescription = document.createElement('div')
    const paragraphDescription = document.createElement('p')
    paragraphDescription.innerText = caption
    cardDescription.classList.add(`description`)
    cardDescription.append(paragraphDescription)
    cardholder.append(cardDescription)

    const cardHeartIcon = document.createElement('div')
    cardHeartIcon.classList.add(`hearticon`)

    const spanIcon = document.createElement('img')
    spanIcon.setAttribute("src", "../icons/heart.svg")
    spanIcon.setAttribute("id", index)
    if (liked) {
        spanIcon.classList.add('invertRed')
    }
    else {
        spanIcon.classList.add(!darkTheme ? 'invertWhite' : 'invertDark')
    }
    spanIcon.addEventListener('click', e => onHeartIconClick(e, spanIcon, card))



    const paragraphHeartIcon = document.createElement('p')
    paragraphHeartIcon.innerText = likes


    cardholder.append(cardHeartIcon)
    cardHeartIcon.append(spanIcon)
    cardHeartIcon.append(paragraphHeartIcon)
}

const printData = (pageIndex, filteredData, flag = true) => {
    currentPage = pageIndex;
    const startRange = (pageIndex - 1) * cardIncrease;
    const endRange = pageIndex * cardIncrease > cardsLimit
        ? cardsLimit
        : pageIndex * cardIncrease;

    if (filteredData && flag) {
        const allCards = document.querySelectorAll('.cardholder')
        for (const card of allCards) {
            card.remove()
        }
        for (let i = 0; i < allCards.length; i++) {
            createCard(filteredData[i], i)
        }

        return
    }
    if (endRange >= cardsLimit) {
        const btnLoadMore = document.querySelector(".btn")
        btnLoadMore.remove()
    }
    if (currentPage === 1) {
        for (let i = 0; i < endRange; i++) {
            createCard(cardsData[i], i)
        }
    } else {
        for (let i = startRange + 1; i <= endRange; i++) {
            createCard(cardsData[i - 1], i)
        }
    }
}

const printAllCard = (pageIndex, filteredData) => {
    const allCards = document.querySelectorAll('.cardholder')
    for (const card of allCards) {
        card.remove()
    }
    const endRange = pageIndex * cardIncrease > cardsLimit
        ? cardsLimit
        : pageIndex * cardIncrease;

    if (filteredData) {
        for (let i = 0; i < filteredData.length; i++) {
            createCard(filteredData[i], i)
        }
    } else {
        for (let i = 0; i < endRange; i++) {
            createCard(cardsData[i], i)
        }
    }
}

const filterCardsByType = (type) => {
    const filteredData = cardsData.filter((card) => card.source_type === type)
    return filteredData
}

document.querySelector('.btn button').addEventListener('click', function (e) {
    e.preventDefault()
    printData(currentPage + 1, cardsData, false);
});

const onHeartIconClick = (e, atribute, value) => {
    e.preventDefault()
    const index = cardsData.findIndex(object => {
        return object === value;
    });
    const FOUNDELEMENT = document.getElementById(`${index}`)
    if (cardsData[index].liked) {
        cardsData[index].likes = String(Number(cardsData[index].likes) - 1)
        cardsData[index].liked = false
        FOUNDELEMENT.classList.add('invertRed')
    } else {
        cardsData[index].likes = String(Number(cardsData[index].likes) + 1)
        cardsData[index].liked = true
        FOUNDELEMENT.classList.remove('invertRed')
    }
    printData(currentPage, cardsData)
}

//TABLET-DESKTOP
const mediaQueryTabletRangeMax = window.matchMedia('(max-width: 991px)')
mediaQueryTabletRangeMax.addListener(changeColumn);
function changeColumn(e) {
    const allCards = document.querySelectorAll('.cardholder')
    if (e.matches) {
        const mediaQueryTabletRangeMin = window.matchMedia('(min-width: 768px)')
        if (mediaQueryTabletRangeMin.matches) {
            for (const card of allCards) {
                card.style[`flex-basis`] = '40%'
            }
        }
    } else {
        for (const card of allCards) {
            card.style[`flex-basis`] = '24%'
        }
    }
}

//MOBILE-TABLET
const mediaQueryPhonetRangeMax = window.matchMedia('(max-width: 767px)');
mediaQueryPhonetRangeMax.addListener(alertMe);
function alertMe(e) {
    const allCards = document.querySelectorAll('.cardholder')
    if (e.matches) {
        for (const card of allCards) {
            card.style[`flex-basis`] = '80%'
        }
    } else {
        for (const card of allCards) {
            card.style[`flex-basis`] = '40%'
        }
    }
}


document.querySelector('#numberOfColumns').addEventListener('change', function (e) {
    const allCards = document.querySelectorAll('.cardholder')

    if (e.target.value === 'dynamic') {
        for (const card of allCards) {
            const mediaQueryTabletRangeMin = window.matchMedia('(min-width: 768px)')
            const mediaQueryTabletRangeMax = window.matchMedia('(max-width: 991px)')

            const mediaQueryPhonetRangeMax = window.matchMedia('(max-width: 767px)')
            if (mediaQueryTabletRangeMin.matches && mediaQueryTabletRangeMax.matches) {
                card.style[`flex-basis`] = '40%'
            }
            else if (mediaQueryPhonetRangeMax.matches) {
                card.style[`flex-basis`] = '80%'
            }
            else {
                card.style[`flex-basis`] = '24%'
            }
        }
    } else {
        for (const card of allCards) {
            card.style[`flex-basis`] = `${100 / e.target.value - 1}%`
        }
    }
});

const onChangeBackgroundColor = (value) => {
    const cards = document.querySelectorAll(".cardholder")
    const reg = /^#([0-9a-f]{3}){1,2}$/i
    for (const card of cards) {
        if (reg.test(value))
            card.style.background = value;
        else
            card.style.background = 'white';
    }
}
const onChangeGap = (value) => {
    const cards = document.querySelectorAll(".cardholder")
    const reg = /^[0-9]{2}px$/i
    const findGapSelector = document.querySelector(".layout-placeholder")
    cards.forEach(() => {
        if (reg.test(value)) findGapSelector.style[`column-gap`] = value
        else findGapSelector.style[`column-gap`] = '10px'
    })
}

document.querySelectorAll('input').forEach((item) => item.addEventListener('input', function (e) {
    if (e.target.id === 'cardSpaceBetween') {
        onChangeGap(e.target.value)
    }
    else if (e.target.id === 'cardBackgroundColor') {
        onChangeBackgroundColor(e.target.value)
    } else return

}))

document.querySelectorAll('.radio-group .radio-select input').forEach((item) => item.addEventListener('click', function (e) {
    switch (e.target.value) {
        case 'all':
            {
                printAllCard(currentPage)
                break;
            }
        case 'lightTheme': {
            darkTheme = false
            printData(currentPage, cardsData)
            const cards = document.querySelectorAll(".cardholder")
            for (const card of cards) {
                card.style.background = "white";
                card.style.color = "black";
            }

            break;
        }
        case 'darkTheme': {
            darkTheme = true
            printData(currentPage, cardsData)
            const cards = document.querySelectorAll(".cardholder")
            for (const card of cards) {
                card.style.background = "black";
                card.style.color = "white";
            }
            break;
        }
        default: {
            const filteredData = filterCardsByType(e.target.value)
            printAllCard(currentPage, filteredData)
        };
    }
}));

const init = async () => {
    cardsData = await getData()
    cardsLimit = cardsData.length
    printData(currentPage)
}
init()


